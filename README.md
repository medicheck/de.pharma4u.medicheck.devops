MC Ansible devops project 
======================================

// TODO describe in short the repo

To prepare a plain Linux Debian Minimal Installation host for a provisioning with ansible do the following

1. Install python as the only ansible prerequisit
```
# apt-get update
# apt-get install python
```
2. Add your public key to the authorized_keys file under /root/.ssh/authorized_keys which uses ansible to connect
```
# mkdir /root/.ssh/
# echo "{key}" > /root/.ssh/authorized_keys
# chmod 0600 /root/.ssh/authorized_keys
```

// TODO describe in short how to use the repo/project to use with ansible to setup srevers and deploy projects
