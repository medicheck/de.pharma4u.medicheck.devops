<?php

/*
 * {{ ansible_managed }}
 */

$database_type = "mysql";
$database_default = "cacti";
$database_hostname = "localhost";
$database_username = "cacti";
$database_password = "{{ cacti.password_db }}";
$database_port = "3306";
$database_ssl = false;

//$url_path = "/cacti/";

$cacti_session_name = "cacti";

?>