#
# {{ ansible_managed }}
#

log_level 4
log_file /var/log/munin/munin-node.log
pid_file /var/run/munin/munin-node.pid

background 1
setsid 1

user root
group root

# This is the timeout for the whole transaction.
# Units are in sec. Default is 15 min
#
# global_timeout 900

# This is the timeout for each plugin.
# Units are in sec. Default is 1 min
#
# timeout 60

# Regexps for files to ignore
ignore_file [\#~]$
ignore_file DEADJOE$
ignore_file \.bak$
ignore_file %$
ignore_file \.dpkg-(tmp|new|old|dist)$
ignore_file \.rpm(save|new)$
ignore_file \.pod$

# Set this if the client doesn't report the correct hostname when
# telnetting to localhost, port 4949
#
#host_name localhost.localdomain

# A list of addresses that are allowed to connect.  This must be a
# regular expression, since Net::Server does not understand CIDR-style
# network notation unless the perl module Net::CIDR is installed.  You
# may repeat the allow line as many times as you'd like

allow ^127\.0\.0\.1$
{% if  muninnode.allow_hosts is defined and muninnode.allow_hosts is not none %}
{% for host in muninnode.allow_hosts %}
allow ^{{ host | replace('.', '\.') }}$
{% endfor %}
{% endif %}

# If you have installed the Net::CIDR perl module, you can use one or more
# cidr_allow and cidr_deny address/mask patterns.  A connecting client must
# match any cidr_allow, and not match any cidr_deny.  Note that a netmask
# *must* be provided, even if it's /32
#
# Example:
#
# cidr_allow 127.0.0.1/32
# cidr_allow 192.0.2.0/24
# cidr_deny  192.0.2.42/32

# Which address to bind to;
host {% if muninnode.host is defined and muninnode.host is not none %}{{ muninnode.host }}{% else %}{{ ansible_eth0.ipv4.address }}{% endif %}

# And which port
port {{ muninnode.port }}

{# Temporarily deacivated encryption to support muninmx which collects from TS' VM #}
{% if inventory_hostname != 'login1.sso.check24.de' and inventory_hostname != 'login2.sso.check24.de' %}
# Enable TLS
tls paranoid
tls_verify_certificate yes
tls_private_key /etc/ssl/login-mgmt/node.key
tls_certificate /etc/ssl/login-mgmt/node.crt
tls_ca_certificate /etc/ssl/login-mgmt/ca.pem
tls_verify_depth 5
{% endif %}