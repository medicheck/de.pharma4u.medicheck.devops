<VirtualHost *:8080>

    ServerName {{ host.www_check24_de }}
    ServerAlias check24.de
    ServerAlias www.check24.com
    ServerAlias check24.com
    ServerAlias www.chekc24.de
    ServerAlias chekc24.de
    ServerAlias www.scheck24.de
    ServerAlias scheck24.de
    ServerAlias www.check24.net
    ServerAlias check24.net
    ServerAlias www.tscheck24.de
    ServerAlias tscheck24.de
    ServerAlias web??.check24.de

    DocumentRoot /var/www/sites/de.check24.www/htdocs

    RewriteEngine On

    RewriteCond %{HTTP:X-HTTPS} !on
    RewriteCond %{HTTP_HOST}    !^www\.check24\.{{ host.tld }} [NC]
    RewriteCond %{HTTP_HOST}    !^web..\.check24\.{{ host.tld }} [NC]
    RewriteRule ^(.*)           http://www.check24.{{ host.tld }}$1 [L,R=301]

    RewriteCond %{HTTP_HOST}    !^www\.check24\.{{ host.tld }} [NC]
    RewriteCond %{HTTP_HOST}    !^web..\.check24\.{{ host.tld }} [NC]
    RewriteRule ^(.*)           https://www.check24.{{ host.tld }}$1 [L,R=301]

    php_value auto_prepend_file /var/www/sites/de.check24.www/lib/includes/_auto_prepend.inc.php
    php_value auto_append_file /var/www/sites/de.check24.www/lib/includes/_auto_append.inc.php
    php_flag short_open_tag on

{% if system.environment == 'production' %}
    SetEnv ENVIRONMENT prod
{% else %}
    SetEnv ENVIRONMENT {{ system.environment }}
{% endif %}
    SetEnv _CacheDir /var/www/sites/_cache_

    <IfModule mod_headers.c>
        Header add Access-Control-Allow-Origin "*"
    </IfModule>

    <Directory "/var/www/sites/de.check24.www/htdocs">
        Options +FollowSymLinks -MultiViews +Includes
        AllowOverride All
        Require all granted
    </Directory>

    CustomLog ${APACHE_LOG_DIR}/access.de.check24.www.log c24

</VirtualHost>
