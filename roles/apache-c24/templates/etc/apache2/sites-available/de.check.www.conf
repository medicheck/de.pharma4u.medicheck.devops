<VirtualHost *:8080>
    ServerName {{ hosts.www_check_de }}
    ServerAlias check.de
    ServerAlias web??.check.de

    DocumentRoot /var/www/sites/de.check/htdocs

    SetEnv CDEENVIRONMENT {{ system.environment }}
    SetEnv CDECACHEDIR /var/www/sites/_cache_

    RewriteEngine On

    {% if system.environment == 'production' %}
    RewriteCond %{HTTPS}        !on
    RewriteCond %{HTTP:X-HTTPS} !on
    RewriteCond %{HTTP_HOST}    !^www\.check\.{{ hosts.tld }} [NC]
    RewriteCond %{HTTP_HOST}    !^web..\.check\.{{ hosts.tld }} [NC]
    RewriteRule ^(.*)           http://www.check.de$1 [L,R=301]

    RewriteCond %{HTTP_HOST}    !^www\.check\.de [NC]
    RewriteCond %{HTTP_HOST}    !^web..\.check\.de [NC]
    RewriteRule ^(.*)           https://www.check.de$1 [L,R=301]
    {% endif %}

    <IfModule mod_headers.c>
        Header add Access-Control-Allow-Origin "*"
    </IfModule>

    <Directory "/var/www/sites/de.check/htdocs">
        AllowOverride All
    </Directory>

    CustomLog ${APACHE_LOG_DIR}/access.de.check.www.log c24

</VirtualHost>
