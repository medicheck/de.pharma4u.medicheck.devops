<?php

    // {{ ansible_managed }}

    //define('RS_CHECK_SPACE_WARNING_USED', '85');
    //define('RS_CHECK_SPACE_CRITICAL_USED', '95');
    define('RS_CHECK_LOGGED_USERS_WARNING', '8');
    define('RS_CHECK_LOGGED_USERS_CRITICAL', '10');
    define('RS_CHECK_LOAD_WARNING', 20);
    define('RS_CHECK_LOAD_CRITICAL', 26);
    define('RS_CHECK_LOAD5_WARNING', 20);
    define('RS_CHECK_LOAD5_CRITICAL', 26);
    define('RS_CHECK_LOAD15_WARNING', 18);
    define('RS_CHECK_LOAD15_CRITICAL', 24);
    //define('RS_CHECK_LOAD_EXCLUDE_INTERVALS', '');
    define('RS_CHECK_MEMORY_USED_WARNING', '90');
    define('RS_CHECK_MEMORY_USED_CRITICAL', '93');
    define('RS_CHECK_MEMORY_SWAP_USED_WARNING', '40');
    define('RS_CHECK_MEMORY_SWAP_USED_CRITICAL', '50');
    //define('RS_CHECK_PROCESS_COUNT_WARNING', '50');
    //define('RS_CHECK_PROCESS_COUNT_CRITICAL', '100');
    //define('RS_CHECK_TIME', '1');
    //define('RS_CHECK_TIME_ZONE', 'Europe/Berlin');
    //define('RS_CHECK_TIME_OFFSET_WARNING', '60');
    //define('RS_CHECK_TIME_OFFSET_CRITICAL', '600');
    define('RS_CHECK_DATABASE_HOST', 'localhost');
    //define('RS_CHECK_DATABASE_PORT', '');
    define('RS_CHECK_DATABASE_USERNAME', 'root');
    define('RS_CHECK_DATABASE_PASSWORD', '{{ percona.user.root.password }}');
    define('RS_CHECK_DATABASE_DATABASE', 'c24login');
    //define('RS_CHECK_RAID_3WARE', 'true');
    //define('RS_CHECK_RAID_3WARE_WRITE_CACHE', '1');
    define('RS_CHECK_RAID_ADAPTEC', false);
    define('RS_CHECK_DATABASE_OPTION_SLAVE', true);
    //define('RS_CHECK_DATABASE_PROCESS_COUNT_WARNING', '50');
    //define('RS_CHECK_DATABASE_PROCESS_COUNT_CRITICAL', '150');
    //define('RS_CHECK_DATABASE_PROCESS_COUNT_LOCKED_WARNING', '25');
    //define('RS_CHECK_DATABASE_PROCESS_COUNT_LOCKED_CRITICAL', '50');
    define('RS_CHECK_FW_PROJECT_ROOT', '/var/www/sites/de.check24.core/default/');
    //define('RS_CHECK_FW_LOGCHECK_IGNORE', '');
    define('RS_CHECK_DIR', '/opt/rscheck/');
    //define('RS_CHECK_SYSTEM', 'debian');
    define('RS_CHECK_SYSTEM_PORTS_ALLOWED_EXTERNAL', '80,443,42022,49049,10061,4758,45306');
    define('RS_CHECK_DAEMON_BIND_IP', '{{ ansible_eth0.ipv4.address }}');
    //define('RS_CHECK_DAEMON_BIND_PORT', '4758');
    //define('RS_CHECK_DAEMON_AUTOUPDATE_TIME', '900');
    //define('RS_CHECK_DAEMON_RESULT_OK_CACHE', '0');
    //define('RS_CHECK_DAEMON_AUTOUPDATE_CHECK_SIGNATURE', '1');
    //define('RS_CHECK_RESULT_SUCCESS', '0');
    //define('RS_CHECK_RESULT_WARNING', '1');
    //define('RS_CHECK_RESULT_CRITICAL', '2');
    define('RS_CHECK_DATABASE_XTRABACKUP_SIZE_TOLERANCE', 0.5);
    define('RS_CHECK_RAID_LSI_NEW', true);
    define('RS_CHECK_RAID_LSI_NEW_STORCLI_BIN_PATH', '/opt/MegaRAID/bin/storcli');
    define('RS_CHECK_RAID_LSI_NEW_CHECK_LSI_RAID_BIN_PATH', '/opt/MegaRAID/bin/check_lsi_raid');
    define('RS_CHECK_RAID_LSI_NEW_DISABLE_BBU_CV_CHECK', false);
?>