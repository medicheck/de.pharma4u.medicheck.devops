server {
    listen {{ host.nginx.ip_address }}:80;
    listen {{ host.nginx.ip_address }}:443 ssl http2;
    server_name m.{{ hosts.check24_de }};
    {% if system.environment != 'production' %}
    server_name *.m.{{ hosts.check24_de }};
    {% endif %}

    ssl_certificate         ssl/{{ cert.de_check24 }}.crt;
    ssl_certificate_key     ssl/{{ cert.de_check24 }}.key;
    ssl_stapling            on;
    ssl_stapling_verify     on;
    ssl_trusted_certificate ssl/{{ cert.de_check24 }}.trusted.crt;
    ssl_dhparam             ssl/dhparams.pem;

    location ~ ^/(imgs|css|js|assets)/ {
        proxy_cache         STATIC;
        proxy_cache_key     "$scheme$host$_accept_encoding$request_uri";
        proxy_cache_valid   200 10m;
        proxy_cache_valid   404 1m;
        proxy_pass          http://localhost:91;
        proxy_no_cache      $http_pragma $http_cache_control;
        proxy_cache_bypass  $_nocache;

        # default header + expire
        include /etc/nginx/proxy_header_settings.conf;
        expires 30d;
    }
    location / {
        proxy_pass http://localhost:91;
        include /etc/nginx/proxy_header_settings.conf;
    }
}
